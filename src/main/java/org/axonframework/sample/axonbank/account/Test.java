package org.axonframework.sample.axonbank.account;

import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.sample.axonbank.coreapi.AccountCreatedEvent;
import org.axonframework.sample.axonbank.coreapi.CreateAccountCommand;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
public class Test {
    @AggregateIdentifier
    private String id;

    @CommandHandler
    public Test(CreateAccountCommand cmd) {
        apply(new AccountCreatedEvent(cmd.getAccountId(), cmd.getOverdraftLimit()));
    }

    @EventSourcingHandler
    protected void on(AccountCreatedEvent event) {
        this.id = event.getAccountId();
        System.out.println("dupa");
    }
}
